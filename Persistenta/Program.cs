﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistenta
{
    class Program
    {
        static void Sortare(int[] v, int n)
        {
            bool contor = false; int aux;
            do
            {
                contor = false;
                for (int i = 0; i < n - 1; i++)
                {
                    if (v[i] > v[i + 1])
                    {
                        aux = v[i];
                        v[i] = v[i + 1];
                        v[i + 1] = aux;
                        contor = true;
                    }
                }
            } while (contor);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti numarul pe care se va calcula persistenta:");
            int numar = int.Parse(Console.ReadLine());
            int contor = 0;
            int p;

            while (numar > 9)
            {
                p = ProdCif(numar);
                numar = p;
                contor++;
            }
            Console.WriteLine(contor);

            Console.ReadKey();
        }

        static int ProdCif(int numar)
        {
            int p = 1;
            while (numar != 0)
            {
                p = p * (numar % 10);
                numar = numar / 10;
            }
            return p;
        }
    }
}
